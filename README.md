CYCLE is an attempt to remake the [LOOP](http://en.wikipedia.org/wiki/LOOP_%28programming_language%29) language in Python using the [pyparsing](http://pyparsing.wikispaces.com/) module 

You will have to install pyparsing by following the instructions [here](http://pyparsing.wikispaces.com/Download+and+Installation)

 a <- a + 2
 b <- b + 2
 c <- c + 1

 cycle a do
  cycle b do
    c<-c+1
  end
 end

 cycle a do
  c<-c+1
 end
 
Output:
 {'a': 2, 'c': 7, 'b': 2}
