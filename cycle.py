'''  
	Copyright 2013 abhijeet bhagat
 
    CYCLE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CYCLE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CYCLE.  If not, see <http://www.gnu.org/licenses/>.
 '''
 
from pyparsing import *
from operator import mul
import argparse
import re


'''
Action to process an assignment statement
'''
def process_assignment(result) :
	global last_expression
	last_expression = ' '.join(result)
	
	if result.lhs_id == result.rhs_id :
		#we are trying to create a new identifier - v <- v + 1
		if result.lhs_id in symbol_table :
			symbol_table[result.lhs_id] = symbol_table[result.lhs_id] + int(result.constant)
		else :
			symbol_table[result.lhs_id] = int(result.constant)
	else :
		if result.rhs_id in symbol_table :
			symbol_table[result.lhs_id] = symbol_table[result.rhs_id] + int(result.constant)
		else :
			raise Exception("rhs id not found")

'''
Action to process a cycle block
'''			
def process_cycle_expression(result):
	for i in range(iteration_count_stack[-1] - 1) :
		assignment_expression.parseString(' '.join(result.partial_program))

def process_cycle_start(result) :
	if result.index_id in symbol_table :
		iteration_count_stack.append(symbol_table[result.index_id])
	else :
		raise Exception("cycle index not found")
	
def process_cycle_end(result) :
	#while iteration_count_stack :
	#	r = iteration_count_stack.pop()
	#	for i in range(r - 1) :
	#		process_assignment(assignment_expression.parseString(last_expression))
	
	#TODO remove this line after testing
	global iteration_count_stack
	
	for i in range(reduce(mul, iteration_count_stack, 1) - 1):
		process_assignment(assignment_expression.parseString(last_expression))
	
	#reset the iteration_count_stack because we are done with the cycle block
	iteration_count_stack = []

#Grammar--	
assignment = '<-'
identifier = Word(alphas)
number	   = Word(nums)
EOL		   = OneOrMore(LineEnd())

mathematical_expression = identifier("rhs_id") + (Literal('+')|Literal('-')) + number("constant")
assignment_expression = identifier("lhs_id") + assignment + mathematical_expression
#assignment_expression.setParseAction(process_assignment)

program = Forward()
cycle_expression = Forward()

cycle_expression_start = 'cycle' + identifier("index_id") + 'do'
#cycle_expression_start.setParseAction(process_cycle_start)

cycle_expression_end = Word('end')
#cycle_expression_end.setParseAction(process_cycle_end)

cycle_expression << cycle_expression_start + Group(assignment_expression | cycle_expression)("partial_program") + cycle_expression_end
#cycle_expression.setParseAction(process_cycle_expression) #not needed

program = (assignment_expression | cycle_expression)("partial_program")
#Grammar--

iteration_count_stack = []
symbol_table = {}
last_expression = ''

if __name__ == "__main__" :
	parser = argparse.ArgumentParser()
	parser.add_argument("file", help = "cycle file for interpretation")
	args = parser.parse_args()
	
	try:
		with open(args.file) as cycle_file :
			source_code = cycle_file.readlines()
			for line in source_code :
				if '<-' in line :
					process_assignment(assignment_expression.parseString(line))
				elif 'cycle' in line :
					process_cycle_start(cycle_expression_start.parseString(line))
				elif 'end' in line :
					process_cycle_end(cycle_expression_end.parseString(line))
				elif re.match(r'^\s*$', line): #ignore blank lines
					continue
				else :
					raise ParseException('Invalid statement encountered: ' + line.rstrip('\n'))
					
		print symbol_table
		
	except ParseException as e:
		print e